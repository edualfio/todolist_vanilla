let request = new XMLHttpRequest();

request.onreadystatechange = () => {
	if (request.readyState === 4) {
		if (request.status === 200) {
			listTask = JSON.parse(request.responseText);
			updateTaskList();
			addLogEntry('Data from server', 'loaded');
		} else {
			console.log(request.status);
		}
	}
};

request.open('Get', 'assets/mock/data.json');

const getData = () => {
	request.send();
};
let logTask = JSON.parse(localStorage.getItem('listLog')) || [];
const targetLog = document.querySelector('.log-list');


const generateLogItem = (log) => {
	const li = document.createElement('li');

	const date = document.createElement('span');
	date.classList.add('date');

	const dateTime = new Date(log.id);
	date.innerText = formatDate(dateTime);

	const info = document.createElement('span');
	info.classList.add('info');
	info.innerHTML = '<strong>' + log.name + '</strong> has been ' + log.action;

	li.appendChild(date);
	li.appendChild(info);
	targetLog.appendChild(li);
};


const addLogEntry = (task, action) => {
	logTask.push({
		id: Date.now(),
		name: task,
		action: action
	});

	updateLogList();
};

const updateLogList = () => {
	targetLog.innerHTML = '';

	// Generamos los items por separado recorriendo el objeto
	for (let log of logTask) {
		generateLogItem(log);
	}

	targetLog.scrollTop = targetLog.scrollHeight;

	// Guardamos en el localStorage los datos para manetener "sesión"
	localStorage.setItem('listLog', JSON.stringify(logTask));
};

const clearLog = () => {
	logTask = [];
	updateLogList();
};

const formatDate = (date) => {
	var monthNames = [
		"January", "February", "March",
		"April", "May", "June", "July",
		"August", "September", "October",
		"November", "December"
	];

	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

	return day + ' ' + monthNames[monthIndex] + ' ' + year + ' | ' + hours + ':' + minutes + ':' + seconds;
};
let listTask = JSON.parse(localStorage.getItem('listTask')) || [];

const target = document.querySelector('.task-list');
const dataEntry = document.querySelector('input[name="newTask"]');
const counter = document.querySelector('.task-counter');

const generateTaskItem = (task) => {
	const li = document.createElement('li');

	const div = document.createElement('div');
	if (task.completed) {
		div.classList.add('task-item', 'active');
	} else {
		div.classList.add('task-item');
	}

	const title = document.createElement('span');
	title.classList.add('title');
	title.innerText = task.name;

	const checkTrigger = document.createElement('a');
	checkTrigger.classList.add('check-trigger');
	checkTrigger.setAttribute('onclick', 'toggleTask(' + task.id + ')');
	checkTrigger.innerHTML = '<img class="check-icon" src="assets/img/check.svg" alt="">';

	const delTrigger = document.createElement('a');
	delTrigger.classList.add('del-trigger');
	delTrigger.setAttribute('onclick', 'delTask(' + task.id + ')');
	delTrigger.innerHTML = '<img class="del-icon" src="assets/img/del.svg" alt="">';

	div.appendChild(title);
	div.appendChild(delTrigger);
	li.appendChild(checkTrigger);
	li.appendChild(div);
	target.appendChild(li);
};


const addTask = () => {
	if (dataEntry.value.length) {
		// Generamos una id Basada en Date
		const id = Date.now();

		// Actualizamos el objeto que controla toda la lista
		listTask.push({
			id: id,
			name: dataEntry.value,
			completed: false
		});

		addLogEntry(dataEntry.value, 'added');
		dataEntry.value = '';
		updateTaskList();
	}

};

const toggleTask = (id) => {
	for (let task of listTask) {
		if (task.id === id) {
			if (!task.completed) {
				addLogEntry(task.name, 'done');
			} else {
				addLogEntry(task.name, 'undone');
			}
			task.completed = !task.completed;
		}
	}

	updateTaskList();
};

const delTask = (id) => {
	for (let task of listTask) {
		if (task.id === id) {
			addLogEntry(task.name, 'deleted');
		}
	}

	listTask = listTask.filter((el) => el.id != id);
	updateTaskList();

};

const updateTaskList = () => {
	target.innerHTML = '';

	// Ordenamos colocando las tareas completadas por encima
	listTask.sort(function(a, b) {
		return b.completed - a.completed;
	});

	// Generamos los items por separado recorriendo el objeto
	for (let task of listTask) {
		generateTaskItem(task);
	}

	// Controlamos el contador de tareas, tanto completas cómo totales
	counter.innerHTML = countCompletes() + '/' + listTask.length;

	updateLogList();

	// Guardamos en el localStorage los datos para manetener "sesión"
	localStorage.setItem('listTask', JSON.stringify(listTask));
};

const countCompletes = () => {
	let count = 0;
	for (let task of listTask) {
		if (task.completed) count++;
	}
	return count;
};

updateTaskList();

// DOM EVENTS
var inputElement = document.querySelector('#addTask');
inputElement.addEventListener("click", addTask);

document.addEventListener("keydown", function(event) {
	if (event.key === "Enter") {
		event.preventDefault();
		addTask();
	}
});