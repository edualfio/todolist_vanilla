let logTask = JSON.parse(localStorage.getItem('listLog')) || [];
const targetLog = document.querySelector('.log-list');


const generateLogItem = (log) => {
	const li = document.createElement('li');

	const date = document.createElement('span');
	date.classList.add('date');

	const dateTime = new Date(log.id);
	date.innerText = formatDate(dateTime);

	const info = document.createElement('span');
	info.classList.add('info');
	info.innerHTML = '<strong>' + log.name + '</strong> has been ' + log.action;

	li.appendChild(date);
	li.appendChild(info);
	targetLog.appendChild(li);
};


const addLogEntry = (task, action) => {
	logTask.push({
		id: Date.now(),
		name: task,
		action: action
	});

	updateLogList();
};

const updateLogList = () => {
	targetLog.innerHTML = '';

	// Generamos los items por separado recorriendo el objeto
	for (let log of logTask) {
		generateLogItem(log);
	}

	targetLog.scrollTop = targetLog.scrollHeight;

	// Guardamos en el localStorage los datos para manetener "sesión"
	localStorage.setItem('listLog', JSON.stringify(logTask));
};

const clearLog = () => {
	logTask = [];
	updateLogList();
};

const formatDate = (date) => {
	var monthNames = [
		"January", "February", "March",
		"April", "May", "June", "July",
		"August", "September", "October",
		"November", "December"
	];

	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();
	var hours = date.getHours();
	var minutes = date.getMinutes();
	var seconds = date.getSeconds();

	return day + ' ' + monthNames[monthIndex] + ' ' + year + ' | ' + hours + ':' + minutes + ':' + seconds;
};