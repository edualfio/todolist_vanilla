let request = new XMLHttpRequest();

request.onreadystatechange = () => {
	if (request.readyState === 4) {
		if (request.status === 200) {
			listTask = JSON.parse(request.responseText);
			updateTaskList();
			addLogEntry('Data from server', 'loaded');
		} else {
			console.log(request.status);
		}
	}
};

request.open('Get', 'assets/mock/data.json');

const getData = () => {
	request.send();
};