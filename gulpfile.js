var gulp = require("gulp");
var sass = require("gulp-sass");
var concat = require("gulp-concat");
var minify = require("gulp-minify");

var browserSync = require('browser-sync')
	.create();


function assets() {
	return (
		gulp.src('assets/**/*')
		.pipe(gulp.dest('build/assets'))
	);
}

function watch() {

	gulp.watch('src/**/*.html', gulp.series(html, reload));
	gulp.watch('src/**/*.js', gulp.series(scripts, reload));
	gulp.watch('src/scss/*.scss', gulp.series(style, reload));

}

function style() {
	return (
		gulp.src('src/scss/*.scss')
		.pipe(sass())
		.on('error', sass.logError)
		.pipe(gulp.dest('build/css'))
	);
}

function scripts() {
	return (
		gulp.src('src/js/*.js')
		.pipe(concat('script.js'))
		.pipe(minify())
		.pipe(gulp.dest('build/js'))
	);
}

function html() {
	return (
		gulp.src('src/**/*.html')
		.pipe(gulp.dest('build/'))
	);
}

function server(done) {
	browserSync.init({
		server: {
			baseDir: 'build'
		}
	});
	done();
}

function reload(done) {
	browserSync.reload();
	done();
}

const dev = gulp.series(assets, html, style, scripts, server, watch);
exports.default = dev;