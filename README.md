
# To Do List

Proyecto realizado con  Javascript Nativo ES6.

## Servidor de desarollo

En consola ejecutamos `npm install` para incorporar los módulos de desarrollo nodeJs.

A continuación ejecutamos el comando `gulp`, que levantará el proyecto en el puerto 3000 (por defecto) de nuestro localhost, y compilará una build en la raíz del proyecto.

## Tips

En la carga inical no hay datos, pero se pueden agregar con una simulación de llamada asíncrona a una API externa. Existe un CTA en la barra de logs para tal efecto. Esta acción borra todos los datos que hayamos añadido previamente.

He incorporado persistencia de datos por browser a traves de `localStorage`.

El log se puede reiniciar en un CTA bajo el titulo de la barra de logs.

## Estructura del código

Por un lado tenemos la carpeta `src` con todos los ficheros de desarrollo y por el otro los assets. Con el proceso gulp integrado, tanto las hojas de estilos SASS como los scripts JS se concatenarán y minificarán en `./build`

## Marcado

La webapp se inicializa desde un index HTML5 con marcado semántico.

## Estilos

Cada uno de los componentes del site tiene su propia subhoja de estilos, importadas desde un `styles.scss`. Toda la maquetación está escrita desde cero, siendo esta responsive, y haciendo uso tanto de grid box como de flex box.


## Scripts

Existren tres ficheros diferentes, el más básico es el encargado de recoger un mockup de datos y cargarlos en el objeto principal de Tareas.

El manager de tareas maneja todo el funcionamiento relacionado con la interacción check, uncheck, add y delete. El manager del log hace lo propio con el registro de actividad de las primeras.


## Limpieza del código

Linters y configuración del editor de base aplicados para todas las tipologías de ficheros.
